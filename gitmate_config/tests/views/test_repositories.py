import os

from IGitt.Interfaces.Repository import Repository as IGittRepository
from rest_framework import status
from rest_framework.reverse import reverse
from social_django.models import UserSocialAuth

from gitmate_config import Providers
from gitmate_config.models import Repository
from gitmate_config.tests.test_base import GitmateTestCase
from gitmate_config.views import RepositoryViewSet


class TestRepositories(GitmateTestCase):

    def setUp(self):
        super().setUp()

        self.repo_list = RepositoryViewSet.as_view(actions={'get': 'list'})
        self.repo_list_url = reverse('api:repository-list')

        self.repo_detail = RepositoryViewSet.as_view(
            actions={'patch': 'partial_update', 'put': 'update'},
        )

    def test_get_repos(self):
        # Clearing a previously created entry from db
        self.repo.delete()

        get_repos_request = self.factory.get(self.repo_list_url)
        response = self.repo_list(get_repos_request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        get_repos_request.user = self.user
        response = self.repo_list(get_repos_request)
        self.assertIn(os.environ['GITHUB_TEST_REPO'],
                      [elem['full_name'] for elem in response.data])
        self.assertIn('plugins', response.data[0])

    def test_activate_repo(self):
        url = reverse('api:repository-detail', args=(self.repo.pk,))
        activate_repo_request = self.factory.patch(
            url,
            {'active': True},
        )
        activate_repo_request.user = self.user
        response = self.repo_detail(activate_repo_request, pk=self.repo.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('https://localhost:8000/webhooks/github',
                      self.repo.igitt_repo().hooks)

        deactivate_repo_request = self.factory.patch(
            url,
            {'active': False},
        )
        deactivate_repo_request.user = self.user
        response = self.repo_detail(deactivate_repo_request, pk=self.repo.pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotIn('https://localhost:8000/webhooks/github',
                         self.repo.igitt_repo().hooks)

    def test_igitt_repo_creation(self):
        igitt_repo = self.repo.igitt_repo()
        self.assertIsInstance(igitt_repo, IGittRepository)

    def test_not_implemented_igitt_repo_creation(self):
        self.auth = UserSocialAuth(
            user=self.user, provider=Providers.GITLAB.value)
        self.auth.set_extra_data({
            'access_token': 'stupidshit'
        })
        self.auth.save()

        with self.assertRaises(NotImplementedError):
            repo = Repository(
                user=self.user,
                provider='gitlab',
                full_name='some_repo',
                active=False
            ).igitt_repo()
