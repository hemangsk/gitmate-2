from django.apps import AppConfig


class GitmateTestpluginConfig(AppConfig):
    name = 'gitmate_testplugin'
    verbose_name = 'Testing'
    description = 'A simple plugin used for testing. Smile :)'
