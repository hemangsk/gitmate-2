from django.apps import AppConfig


class GitmateWelcomeCommenterConfig(AppConfig):
    name = 'gitmate_welcome_commenter'
    verbose_name = 'PR Autoresponding'
    description = 'Autoresponds to PRs/MRs.'
